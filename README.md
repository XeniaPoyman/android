# README #

This repository contains some of my applications for Android OS.

The BUTTON is my first application which is a simple calculator.

The second one is TipCalculator. This application allows you to count  tips. A user can set a percentage, and enter the total amount of the check.

The last one Android App - Weather that returns a weather forecast for 16 days for a given city. To obtain the weather forecast, were used HttpURLConnection,
which allows you to make a request to a REST-compatible web-service http://openweathermap.org; and JSON, returned by this weather web service.

