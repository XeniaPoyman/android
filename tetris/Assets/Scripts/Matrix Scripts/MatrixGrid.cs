﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatrixGrid  {

    public static int raw = 10;
    public static int colomn = 20;

    public static Transform[,] grid = new Transform[raw, colomn];
    public static Vector2 RoundVector(Vector2 v)
    {
        return new Vector2(Mathf.Round(v.x), Mathf.Round(v.y));
    }

    public static bool IsInBorder(Vector2 pos)
    {
        return ((int)pos.x >= 0 && (int)pos.x < raw && (int)pos.y >= 0);
    }

    public static void DeleteRaw(int y)
    {
        for(int x = 0; x < raw; ++x)
        {
            GameObject.Destroy(grid[x, y].gameObject);
            grid[x, y] = null;
        }

    }

    public static void DecreaseRow(int y)
    {
        for(int x = 0; x < raw; ++x)
        {
            if(grid[x, y] != null)
            {
                grid[x, y - 1] = grid[x, y];
                grid[x, y] = null;

                grid[x, y - 1].position += new Vector3(0, -1, 0);
            }
        }
    }

    public static void DecreasRowsAbove(int y)
    {
        for(int i = y; i < colomn; ++i)
        {
            DecreaseRow(i);
        }
    }

    public static bool IsFullRow(int y)
    {
        for(int x = 0; x < raw; ++x)
        {
            if(grid[x, y] == null)
            {
                return false;
            }
        }
        return true;
    }

    public static void DeleteWholeRow()
    {
        for(int y = 0; y < colomn; ++y)
        {
            if (IsFullRow(y))
            {
                DeleteRaw(y);
                DecreasRowsAbove(y + 1);
                --y;
            }
        }
    }
}
