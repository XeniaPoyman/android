﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    [SerializeField]
    private GameObject[] tetrisObgects;

    private void Start()
    {
        SpawnRandom();
    }

    public void SpawnRandom()
    {
        int index = Random.Range(0, tetrisObgects.Length);
        Instantiate(tetrisObgects[index], transform.position, Quaternion.identity);
    }
}
