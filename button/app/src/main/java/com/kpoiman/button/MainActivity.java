package com.kpoiman.button;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btOne, btTwo, btThree, btFour, btFive, btSix, btSeven, btEight, btNine, btZero;
    Button btPlus, btMin, btMult, btDev, btEuals, btClear;
    TextView m_textView;
    double operand1, operand2, result;
    int action;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btOne = (Button) findViewById(R.id.buttonOne);
        btTwo = (Button) findViewById(R.id.buttonTwo);
        btThree = (Button) findViewById(R.id.buttonThree);
        btFour = (Button) findViewById(R.id.buttonFour);
        btFive = (Button) findViewById(R.id.buttonFive);
        btSix = (Button) findViewById(R.id.buttonSix);
        btSeven = (Button) findViewById(R.id.buttonSeven);
        btEight = (Button) findViewById(R.id.buttonEight);
        btNine = (Button) findViewById(R.id.buttonNine);
        btZero = (Button) findViewById(R.id.buttonZero);
        btPlus = (Button) findViewById(R.id.buttonPlus);
        btMin = (Button) findViewById(R.id.buttonMin);
        btMult = (Button) findViewById(R.id.buttonMult);
        btDev = (Button) findViewById(R.id.buttonDev);
        btEuals = (Button) findViewById(R.id.buttonEuals);
        btClear = (Button) findViewById(R.id.buttonClear);
        m_textView = (TextView) findViewById(R.id.textView);

        btOne.setOnClickListener(this);
        btTwo.setOnClickListener(this);
        btThree.setOnClickListener(this);
        btFour.setOnClickListener(this);
        btFive.setOnClickListener(this);
        btSix.setOnClickListener(this);
        btSeven.setOnClickListener(this);
        btEight.setOnClickListener(this);
        btNine.setOnClickListener(this);
        btZero.setOnClickListener(this);
        btPlus.setOnClickListener(this);
        btMin.setOnClickListener(this);
        btMult.setOnClickListener(this);
        btDev.setOnClickListener(this);
        btEuals.setOnClickListener(this);
        btClear.setOnClickListener(this);

        operand1 = 0;
        operand2 = 0;
        action = 0;
        result = 0;
        m_textView.setText(Double.toString(operand1));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonOne:
                ClickNumber(1);
                break;
            case R.id.buttonTwo:
                ClickNumber(2);
                break;
            case R.id.buttonThree:
                ClickNumber(3);
                break;
            case R.id.buttonFour:
                ClickNumber(4);
                break;
            case R.id.buttonFive:
                ClickNumber(5);
                break;
            case R.id.buttonSix:
                ClickNumber(6);
                break;
            case R.id.buttonSeven:
                ClickNumber(7);
                break;
            case R.id.buttonEight:
                ClickNumber(8);
                break;
            case R.id.buttonNine:
                ClickNumber(9);
                break;
            case R.id.buttonZero:
                ClickNumber(0);
                break;
            case R.id.buttonPlus:
                if (action == 0) action = 1;
                break;
            case R.id.buttonMin:
                if (action == 0) action = 2;
                break;
            case R.id.buttonMult:
                if (action == 0) action = 3;
                break;
            case R.id.buttonDev:
                if (action == 0) action = 4;
                break;
            case R.id.buttonEuals:
                switch (action) {
                    case 1:
                        result = operand1 + operand2;
                        break;
                    case 2:
                        result = operand1 - operand2;
                        break;
                    case 3:
                        result = operand1 * operand2;
                        break;
                    case 4:
                        result = operand1 / operand2;
                        break;
                    default:
                        Toast.makeText(this, "Операция не задана", Toast.LENGTH_LONG);
                }
                if (action != 0) {
                    m_textView.setText(Double.toString(result));
                    operand1 = 0;
                    operand2 = 0;
                    result = 0;
                    action = 0;
                }
                break;
            case R.id.buttonClear:
                operand1 = 0;
                operand2 = 0;
                result = 0;
                action = 0;
                m_textView.setText(Double.toString(operand1));
                break;


        }
    }

    private void ClickNumber(int num){
        if(action == 0){
            operand1 = operand1*10+num;
            m_textView.setText(Double.toString(operand1));
        }else {
            operand2 = operand2*10+num;
            m_textView.setText(Double.toString(operand2));
        }
    }
}
